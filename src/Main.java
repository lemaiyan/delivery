public class Main {
    private static int COST_PER_KM = 30;

    private static Route getAllLocations(){
        Route allLocations = new Route();
        allLocations.addLocation(new Location("X", -1.300176, 36.776714,0.0));
        allLocations.addLocation(new Location("Y", -1.300191, 36.776801,5.0));
        allLocations.addLocation(new Location("Z", -1.300201, 36.776901,3.0));
        return allLocations;
    }

    private static Order orderA(){
        Route allLocations = getAllLocations();
        Location origin = allLocations.getLocationGeolocation(-1.300176,36.776714);
        Location destination = allLocations.getLocationGeolocation(-1.300191, 36.776801);
        Order order = new Order("A", origin, destination);
        order.setCost(order.getDistance() * COST_PER_KM);
        return order;
    }

    private static Order orderB(){
        Route allLocations = getAllLocations();
        Location origin = allLocations.getLocationGeolocation(-1.300176,36.776714);
        Location destination = allLocations.getLocationGeolocation(-1.300201, 36.776901);
        Order order = new Order("B", origin, destination);
        order.setCost(order.getDistance() * COST_PER_KM);
        return order;
    }

    public static void main(String[] args){
        Route allLocations = getAllLocations();
        // Create a new Rider
        Rider riderA = new Rider("Rider A");
        // At this point the orderA is placed
        Order orderA = orderA();
        riderA.addOrder( orderA);
        riderA.setOnTrip(true);
        // a minute later order B is placed
        Order orderB = orderB();

        /*
        Assumption here os that both orders are placed from the same
        location. Else we would poll the routes database to get the
        routes that have the same origin or within a given geo location radius
        and are placed within a given period of time or where the original
        order hasn't been moved over a distance to which going back to pick
        the other order doesn't cost the first customer extra and doesn't
        exceed the distance from destination of the first order to the
        destination of the next order.
         */

        // check you can get to the destination of the second order
        // from the destination of first_order
        Location orderADestination = allLocations.getLocationGeolocation(-1.300191, 36.776801);
        // if it's possible to get to the location
        boolean isPossible = false;
        while(true){
            if(orderADestination.getGeolocation().equals(orderB.getDestination().getGeolocation())){
                isPossible = true;
                break;
            }
            if(orderADestination.getNext() == null) break;
            orderADestination = orderADestination.getNext();
        }
        // if is possible we assign the rider the new order
        if(isPossible) riderA.addOrder(orderB);

        System.out.println(riderA.toString());
    }
}
