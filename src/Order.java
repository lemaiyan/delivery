public class Order {
    private String orderId;
    private Location origin;
    private Location destination;
    private double cost;
    private Route orderRoute;

    /***
     * Constructor
     * @param orderId OrderID
     * @param origin origin location
     * @param destination destination location
     */
    public Order(String orderId, Location origin, Location destination) {
        this.orderId = orderId;
        this.origin = origin;
        this.destination = destination;
        this.orderRoute = new Route();
        this.orderRoute.addLocation(origin);
        this.orderRoute.addLocation(destination);
    }

    /***
     * Get Order Id
     * @return String
     */
    public String getOrderId() {
        return orderId;
    }

    /***
     * Get Order Origin
     * @return Location
     */
    public Location getOrigin() {
        return origin;
    }

    /***
     * get Order Destination
     * @return Location
     */
    public Location getDestination() {
        return destination;
    }

    /***
     * Get Order Cost
     * @return double
     */
    public double getCost() {
        return cost;
    }

    /***
     * Set Order Cost
     * @param cost double
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /***
     * Get the distance of the order
     * @return double
     */
    public double getDistance(){
        double distance = 0;
        Location currentLocation = this.origin;
        while(true){
            currentLocation = currentLocation.getNext();
            distance += currentLocation.getDistance();
            if(currentLocation.equals(this.destination)) break;
        }
        return distance;
    }

    /***
     * Get Order Route
     * @return Route
     */
    public Route getOrderRoute() {
        return orderRoute;
    }

    /***
     * Override to String method
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder stringValue = new StringBuilder("{");
        stringValue.append("orderId: "+ this.getOrderId());
        stringValue.append(", origin: "+ this.getOrigin());
        stringValue.append(", destination: "+ this.getDestination());
        stringValue.append(", distance: "+ this.getDistance());
        stringValue.append(", cost: "+ this.getCost());
        stringValue.append("}");
        return stringValue.toString();
    }
}
