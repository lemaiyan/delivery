import java.util.ArrayList;

public class Rider {
    private ArrayList<Order> orders = new ArrayList<>();
    private String name;
    private boolean isOnTrip;

    /***
     * Constructor
     * @param name String
     */
    public Rider(String name) {
        this.name = name;
    }

    /***
     * Get Rider Orders
     * @return ArrayList
     */
    public ArrayList<Order> getOrders() {
        return orders;
    }

    /***
     * Assign order to rider
     * @param order Order
     */
    public void addOrder(Order order) {
        this.orders.add(order);
    }

    /***
     * Get rider name
     * @return String
     */
    public String getName() {
        return name;
    }

    /***
     * Set rider name
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Check if rider is on trip
     * @return boolean
     */
    public boolean isOnTrip() {
        return isOnTrip;
    }

    /***
     * Set the rider is on trip
     * @param onTrip boolean
     */
    public void setOnTrip(boolean onTrip) {
        isOnTrip = onTrip;
    }

    /***
     * Override toString method
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder stringValue = new StringBuilder("{");
        stringValue.append("name:"+ this.getName());
        stringValue.append(",orders:"+ this.getOrders());
        return stringValue.toString();
    }
}
