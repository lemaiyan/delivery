public class Location {

    private double latitude;
    private double longitude;
    private String name;
    private double distance;
    private Location next;
    private Geolocation geolocation;

    /***
     * Constructor for the Location class
     * @param name the name of the location
     * @param latitude the latitude of the location
     * @param longitude the longitude of the location
     * @param distance the distance of the location from the previous location
     *                 for the first location the distance will be 0
     */
    public Location(String name, double latitude, double longitude, double distance){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.geolocation = new Geolocation(this.latitude, this.longitude);
    }

    /***
     * Get Location Geolocation
     * @return Geolocation
     */
    public Geolocation getGeolocation() {
        return geolocation;
    }

    /***
     * Get Location latitude
     * @return double
     */
    public double getLatitude() {
        return latitude;
    }

    /***
     * Set location latitude
     * @param latitude double
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /***
     * Get location longitude
     * @return double
     */
    public double getLongitude() {
        return longitude;
    }

    /***
     * Get location Longitude
     * @param longitude double
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /***
     * Get location name
     * @return String
     */
    public String getName() {
        return name;
    }

    /***
     * Set location Name
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Get location distance
     * @return double
     */
    public double getDistance() {
        return distance;
    }

    /***
     * Set location distance
     * @param distance double
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    /***
     * Get the next location linked to this location
     * @return Location
     */
    public Location getNext() {
        return next;
    }

    /***
     * Set next location linked to this location
     * @param next Location
     */
    public void setNext(Location next) {
        this.next = next;
    }

    /***
     * Override toString method
     * @return
     */
    @Override
    public String toString() {
        StringBuilder stringValue = new StringBuilder("{");
        stringValue.append("name: "+ this.getName());
        stringValue.append(", latitude: "+ this.getLatitude());
        stringValue.append(", longitude: "+ this.getLongitude());
        stringValue.append(", distance: "+ this.getDistance());
        stringValue.append("}");
        return stringValue.toString();
    }
}
