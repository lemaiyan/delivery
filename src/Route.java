public class Route{
    // routeTable to hold the linked location an array with only one element
    private Location [] routeTable = new Location[1];

    /***
     * Add a new location to the linked list
     * @param location the location to add
     */
    public void  addLocation(Location location){
        // if this is the first location
        Location currentLocation = routeTable[0];
        if(currentLocation == null){
            routeTable[0] = location;
        }
        else{
            /*
             * we already have locations in our route Table
             * so we append the location at the tail of our linked locations
             */
            while(true){
                // break if location list doesn't have a next location
                if(currentLocation.getNext() == null) break;
                // else we set the location to the next location.
                currentLocation = currentLocation.getNext();
            }
            //set the next of the current location to the next location
            currentLocation.setNext(location);
        }
    }

    /***
     * Get the location by name
     * @param name the name of the location
     * @return Location
     */
    public Location getLocationByName(String name){
        Location currentLocation = routeTable[0];
        if(currentLocation == null){
            return null;
        }else{
            /*
             * loop through the linked locations
             */
            while(true){
                // if we get a location with the name return that location
                if(currentLocation.getName().equals(name)){
                    return  currentLocation;
                }
                // no other location exists we break
                if(currentLocation.getNext() == null) break;
                // else set current location as the next location
                currentLocation = currentLocation.getNext();
            }
        }
        return null;
    }

    /***
     * Get Location by geolocation
     * @param latitude double
     * @param longitude double
     * @return Location
     */
    public Location getLocationGeolocation(double latitude, double longitude){
        Location currentLocation = routeTable[0];
        if(currentLocation == null){
            return null;
        }else{
            /*
             * loop through the linked locations
             */
            while(true){
                // if we get a location with the name return that location
                if(currentLocation.getLatitude() == latitude &&
                        currentLocation.getLongitude() == longitude){
                    return  currentLocation;
                }
                // no other location exists we break
                if(currentLocation.getNext() == null) break;
                // else set current location as the next location
                currentLocation = currentLocation.getNext();
            }
        }
        return null;
    }
}
